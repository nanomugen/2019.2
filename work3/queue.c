#include "index.h"
#include <pthread.h>

void QueuePush(struct Node* node){
  //uint64_t diff;
  //struct timespec tick, tock;
  //clock_gettime(CLOCK_REALTIME, &tick);
  if(queue == NULL) return;
  struct Atom *atom = malloc(sizeof(struct Atom));
  atom->node = node;
  atom->next=NULL;
  pthread_mutex_lock(&queue_mutex);
  if(queue->next==NULL){
    queue->next=atom;
  }
  else{
    queue->last->next = atom;
  }
  queue->last = atom;
  queue->size++;
  if(queue->size > queue->maxSize)queue->maxSize++;
  queue->sum+=queue->size;
  queue->count++;
  //printf("%ld>%ld>%ld\n", (*queue)->count,(*queue)->size,(*queue)->sum);
  pthread_cond_signal(&pop_cond);
  pthread_mutex_unlock(&queue_mutex);
  //clock_gettime(CLOCK_REALTIME, &tock);
  //diff = NANOS * (tock.tv_sec - tick.tv_sec) + tock.tv_nsec - tick.tv_nsec;
  //pthread_mutex_lock(&push_mutex);
  //push_time+=(double)diff/NANOS;
  //pthread_mutex_unlock(&push_mutex);
}

struct Node* QueuePop(){
  //printf("%d::entrei no pop\n",pthread_self());
  if(queue == NULL) return;
  pthread_mutex_lock(&queue_mutex);
  //((*queue)->inactive)++;
  while((queue)->next==NULL){
    //((*queue)->inactive)--;
    //printf("%d::entrei no next==NULL(não achei nada)\n",pthread_self());
    if((queue->active)-- <0){
      pthread_mutex_unlock(&queue_mutex);
      pthread_cond_broadcast(&pop_cond);
      //printf("retornou o kill &&&&&&&&&&&&&&&&&&&&&&\n");
      return kill;
    }
    
    pthread_cond_wait(&pop_cond,&queue_mutex);
    (queue->active)++;
  }
  (queue->inactive)--;
  struct Node* node = (queue->next->node);
  struct Atom* atom = queue->next->next;
  free(queue->next);
  queue->next=atom;
  queue->size--;
  //printf("%ld<%ld<%ld\n", (*queue)->count,(*queue)->size,(*queue)->sum);
  pthread_mutex_unlock(&queue_mutex);

  return node;
}



void QueueInit() {
    queue = calloc(1, sizeof(struct Queue));
    pthread_mutex_init(&queue_mutex,NULL);
    pthread_cond_init(&pop_cond,NULL);
}


void AtomKill(struct Atom** atom){
  struct Atom* aux;
  int test=0;
  if((*atom) != NULL){
    test=1;
    aux = (*atom)->next;
  }
  free (*atom);
  if(test==1)
    AtomKill(&aux);
}

void QueueKill(){
  AtomKill(&(queue->next));
  free(queue);
  queue = NULL;
  pthread_mutex_destroy(&queue_mutex);
  pthread_cond_destroy(&pop_cond);
}
